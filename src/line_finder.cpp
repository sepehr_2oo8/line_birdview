#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/Image.h"

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
 #include <sensor_msgs/image_encodings.h>
//opencv libraries
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/ml/ml.hpp"
#include <cv.h>
#include <cxcore.h>
#include <highgui.h>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/core/core.hpp>
#include "opencv2/core/core_c.h"
#include "opencv2/core/internal.hpp"
#include "opencv2/features2d/features2d.hpp"

//standard libraries
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <math.h>
#include <sys/time.h>
#include <stdio.h>
#include <string>

#define IMAGES_TAKEN_COUNT 30
#define CAMERA_APERTURE 100 // in CM
#define INSPECTING_LINE_WIDTH 4	//in CM
#define DESIRED_DISTANCE_FROM_LINE 200 //in CM

using namespace cv;
using namespace std;


float twoPointDst(Point p1 , Point p2){
    float d=0;
    d=sqrt((float) ( pow((float) p1.x-p2.x,2)+pow((float) p1.y-p2.y,2)));
    return d;
}

class inspecObj {
public:
    inspecObj(Mat img){originalImg=img.clone();}
    void setParams (float CA , float LW){cameraAp = CA; lineWidth=LW;}
    vector <float> calcDisplacement(Mat prvImg,vector <KeyPoint> prvKeyPts ,float prvDist);
    Mat originalImg;
    Mat lineImg;
    Mat descriptor;
    Mat prevDescriptor;
    Mat ROI ;
    Mat valid_ROI;
    Mat valid_image;
    Point2f center;
    float width;
    float distance;
    float lineWidth; // CM
    vector <KeyPoint> keyPoints;
    vector <KeyPoint> prevKeypoints;
    float cameraAp;//vertical span in a meter distance (CM)
    Rect lineBounds;
    Mat pre , pro;
    Mat preProcess();
    Mat processing();
    int extraProccess();
};

Mat inspecObj::preProcess(){
    namedWindow ("pre",CV_WINDOW_NORMAL);
    int i,j,k,flag;
    Mat fnImg=originalImg.clone();
    Mat temp=Mat::zeros(fnImg.size(),CV_8UC3);
    Mat temp1=Mat::zeros(fnImg.size(),CV_8UC1);
    Mat temp2=Mat::zeros(fnImg.size(),CV_8UC1);
    Mat temp3=Mat::zeros(fnImg.size(),CV_8UC1);
    Mat out=Mat::zeros(fnImg.size(),CV_8UC1);
    Mat element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(11, 11));

    fnImg.convertTo(temp,CV_8UC1);
    //bilateralFilter(fnImg,temp,10,40,5);
    cvtColor(temp,temp,CV_BGR2GRAY);
    //pyrDown(temp,temp);
    //pyrDown(out,out);
    Canny(temp,temp1,80,200);
    adaptiveThreshold(temp,temp2,255,ADAPTIVE_THRESH_MEAN_C,THRESH_BINARY_INV,25,0);
    threshold(temp,temp3,80,255,THRESH_BINARY_INV);

    for (j=1;j<temp1.rows-1;j++){
        for (i=1;i<temp1.cols-1;i++){
            if (temp1.at<unsigned char> (j,i) >0){
                if (( temp.at<unsigned char> (j-1,i-1) + temp.at<unsigned char> (j-1,i) + temp.at<unsigned char> (j-1,i+1)) > 1.5*( temp.at<unsigned char> (j+1,i-1) + temp.at<unsigned char> (j+1,i) + temp.at<unsigned char> (j+1,i+1)) ){
                    out.at<unsigned char> (j+1,i-1)=255;
                    out.at<unsigned char> (j+1,i)=255;
                    out.at<unsigned char> (j+1,i+1)=255;
                }
                if (( temp.at<unsigned char> (j-1,i-1) + temp.at<unsigned char> (j-1,i) + temp.at<unsigned char> (j-1,i+1)) <  0.66*( temp.at<unsigned char> (j+1,i-1) + temp.at<unsigned char> (j+1,i) + temp.at<unsigned char> (j+1,i+1)) ){
                    out.at<unsigned char> (j+1,i-1)=255;
                    out.at<unsigned char> (j+1,i)=255;
                    out.at<unsigned char> (j+1,i+1)=255;
                }
            }
        }
    }
    dilate(out,out,element);

    pre=out.clone();
    imshow ("pre",out);
    return out;
}
Mat inspecObj::processing(){
    int i,j;
    namedWindow ("pro",CV_WINDOW_NORMAL);
    Mat fnImg=pre.clone();
    Mat show=Mat::zeros(fnImg.size(),CV_8UC3);
    vector <vector <Point> > contours;
    vector <Vec4i> hierarchy;
    vector <double> lengths;
    vector <Point> massCenters;
    vector <Moments> mo;
    vector <Point> approx;
    vector <Point> realSizeApprox;
    ROI=Mat::zeros(originalImg.size(),CV_8UC1);
    int maxL;
    float alpha=0;

    findContours(fnImg,contours,hierarchy,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE );

    // Error handling
    Mat emptyMat;
    if (contours.size()==0)
        return emptyMat;
    ////////////////
    if (contours.size()==0)
        cout << "No contour has found!\n";
    for (i=0;i<contours.size();i++)
        drawContours(show,contours,i,Scalar(rand()%255,rand()%255,rand()%255),1);
    for (i=0;i<contours.size();i++){
        mo.push_back(moments(contours[i]));
        lengths.push_back(arcLength(contours[i],true));
        massCenters.push_back(Point2f( mo[i].m10/mo[i].m00 , mo[i].m01/mo[i].m00 ));
        circle(show,massCenters[i],2,Scalar(rand()%255,rand()%255,rand()%255),2);
        //cout << i << "    length=" << lengths[i] <<"   centers=" << massCenters[i] <<endl;
    }
    maxL=0;
    for (i=0;i<lengths.size();i++)
        if (lengths[maxL]<lengths[i])
            maxL=i;
    center=massCenters[maxL];
    width = 2*2*contourArea(contours[maxL],false)/lengths[maxL];
//    cout << "width="<<width << endl;
    alpha = ( 2*(180/3.14)*atan( (float) cameraAp/200 )  / (originalImg.rows)) * width ;
    distance=lineWidth* 1/tan (alpha*3.14/180);
//    cout << "distance=" << distance << endl ;

    approxPolyDP(contours[maxL],approx,3,true);
    for (i=0;i<approx.size();i++)
        realSizeApprox.push_back(approx[i]*1);
    const Point* elementPoints[1] = { &realSizeApprox[0] };
    int numberOfPoints = (int)approx.size();
//    cout << "numberOfPoints=" << numberOfPoints << endl;
    fillPoly(ROI,elementPoints,&numberOfPoints,1,Scalar(255,0,0));
    if (ROI.empty())
        cout << "ROI is empty" <<endl;
    //pyrUp(ROI,ROI);
    //lineBounds=boundingRect(approx);
    //rectangle(show,lineBounds,Scalar(20,20,150),2);
    //lineImg=originalImg(Rect(2*lineBounds.x,2*lineBounds.y,2*lineBounds.width,2*lineBounds.height));
    pro=show.clone();
    imshow ("pro",show);
    return show;
}
int inspecObj::extraProccess(){
    Mat fnImg=originalImg.clone();
    Mat fnROI=ROI.clone();
    //pyrDown(fnROI,fnROI);
    int i,k;
//    cout << "prev keypoints size=" << prevKeypoints.size() << endl ;
    //Mat cropped;
    //Mat fnImg=originalImg(Rect(2*lineBounds.x,2*lineBounds.y,2*lineBounds.width,2*lineBounds.height));
    //cvtColor (fnImg,fnImg,CV_RGB2GRAY);
    //pyrDown(fnImg,fnImg);
    for (i=0;i<5;i++){
        SURF surf(300-i*65);
        surf.detect(fnImg,keyPoints,fnROI);//,ROI);
        surf.compute(fnImg,keyPoints,descriptor);
        if (keyPoints.size()==0){
            cout << "couldn't find keyPoints\n";
            continue;
            if (i==4)
                return -1;
        }
        if (keyPoints.size() > 0){
            valid_ROI=fnROI.clone();
            valid_image=fnImg.clone();
            prevKeypoints.clear();
            for (k=0;k<keyPoints.size();k++ )
                prevKeypoints.push_back(keyPoints[k]);
            prevDescriptor=descriptor.clone();
//            cout << "key points has found and prevKeypoints size=" << prevKeypoints.size() << endl  ;
            break;
        }
    }
    if (keyPoints.size()==0){
        namedWindow("ROI test" , CV_WINDOW_NORMAL);
        //imshow ("ROI test" , fnROI);
        keyPoints=prevKeypoints;
        descriptor=prevDescriptor;
        cout << "couldn't detect keypoints	we end up with prev; size=" << keyPoints.size() << endl ;

    }
    //namedWindow("keyPoints" , CV_WINDOW_NORMAL);
    //drawKeypoints(fnImg,keyPoints,fnImg,Scalar (150,150,10));
    //imshow("keyPoints",fnImg);
    return 0;
}

vector <float> inspecObj::calcDisplacement(Mat prvImg,vector <KeyPoint> prvKeyPts ,float prvDist ){
    vector <float> disPlace (3);
    disPlace [0]=0; disPlace [1]=0; disPlace [2]=0;
    if (prvKeyPts.empty())
        return disPlace;

    float errMean=0;
    int i,j,count=0;
    Mat g1,g2;
    cvtColor (prvImg,g1,CV_RGB2GRAY);
    //pyrDown(g1,g1);
    cvtColor (originalImg,g2,CV_RGB2GRAY);
    //pyrDown(g2,g2);
    vector <unsigned char> status(prvKeyPts.size());
    vector <float> error(prvKeyPts.size());
    vector <Point2f> nextPts(prvKeyPts.size());
    vector <Point2f> prvPts(prvKeyPts.size());
    for (int i=0;i<prvKeyPts.size();i++)
        prvPts[i]=prvKeyPts[i].pt;
    TermCriteria crit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS,20,0.03);
    disPlace[2]= distance-prvDist;
    //extraProccess();
    calcOpticalFlowPyrLK(g1,g2,prvPts,nextPts,status,error,Size (5,5),6,crit,OPTFLOW_USE_INITIAL_FLOW);
    Mat m=originalImg.clone();
    //pyrDown(m,m);
    namedWindow ("matched",CV_WINDOW_NORMAL);
    for (i=0;i<prvKeyPts.size();i++){
        if(status[i]!=0){
            errMean+=error[i];
            count++;
        }
    }
    if (count!=0)
        errMean=errMean/count;

    Point2f p(0,0);
    count=0;
    for (i=0;i<prvKeyPts.size();i++){
        if(status[i]!=0 && error[i] < errMean){
            p+=nextPts[i]-prvPts[i];
            circle (m,prvPts[i],3,Scalar(150,10,10),2);
            circle (m,nextPts[i],3,Scalar(10,10,150),2);
            line (m,prvPts[i],nextPts[i],Scalar(10,150,10),2);
            count++;
        }
    }
    imshow ("matched",m);
    if (count!=0){
        disPlace[0]=p.x/count;
        disPlace[1]=p.y/count;
    }
    cout << endl <<"dx=" << disPlace[0] << "	dy=" <<disPlace[1]<< "	dz=" << disPlace[2]<<endl;
    return disPlace;
}

class Hinspect {
public:
    vector <Mat> trainDescriptors;
    vector <KeyPoint> goodKeys;
    vector <Mat> trainImgs;
    vector <vector <KeyPoint> >  trainKeyPts;
    vector <DMatch> matches;
    Vec4f rebuildMask(Mat mask,float widthInPix,vector <KeyPoint> queryKeys);
    Hinspect(vector <Mat> train_descriptors,vector <vector <KeyPoint> > train_keyPoints){trainDescriptors=train_descriptors; trainKeyPts=train_keyPoints;}
    void findKeyPts();
    void match(Mat  queryDescriptor);
};

void Hinspect::findKeyPts(){

    for (int i=0;i<trainImgs.size();i++){

        inspecObj ins(trainImgs[i]);
        ins.setParams(150,3);
        ins.preProcess();
        ins.processing();
        ins.extraProccess();
        if (ins.keyPoints.size()==0)
            cout << "ins.keyPoints is zero do something!\n";
        trainDescriptors.push_back(ins.descriptor);
        trainKeyPts.push_back(ins.keyPoints);

    }
}

void Hinspect::match(Mat queryDescriptor){
    if (queryDescriptor.empty()){
        cout << "Hins match queryDescriptor is empty\n";
    }

    BFMatcher matcher(5);
    int counter=0;
    matcher.add(trainDescriptors);
    vector <DMatch> goodMatches;
    matcher.train();
 //   cout << "trying to match\n";
    matcher.match(queryDescriptor,matches);
 //   cout << "matching\n";
    if (matches.size()==0)
            cout << "matches.size=" << matches.size();
    for (int i=0;i<matches.size();i++)
        if (matches[i].distance < 0.2)
            goodMatches.push_back(matches[i]);
    while (goodMatches.size()==0){
        counter++;
        for (int i=0;i<matches.size();i++)
            if (matches[i].distance < 0.2+counter/10)
                goodMatches.push_back(matches[i]);
        if (counter>10){
            cout << "there is something wrong with image!\n";
            break;
        }
    }
    if (goodMatches.size()==0)
            cout << "good matches size is zero do something!\n";
    matches=goodMatches;
}

Vec4f Hinspect::rebuildMask( Mat mask,float width,vector <KeyPoint> queryKeys){
    int i,j,k,m,n;
    vector <Point2f> goodPoints;
    if (queryKeys.empty()){

        cout << "Hins rebuildMask queryKeys is empty\n";
    }
    for (i=0 ; i <matches.size();i++){
        goodKeys.push_back(queryKeys[matches[i].queryIdx]);
        goodPoints.push_back(goodKeys[i].pt);
    }
    Vec4f goodLine;
    fitLine(goodPoints,goodLine,CV_DIST_L2,0,0.01,0.01);
    float x,y,a1,a2,theta;
    float temp;
    a1=goodLine[1]/goodLine[0];
    Point lineStart=Point (goodLine[2]-200*goodLine[0],goodLine[3]-200*goodLine[1]);
    Point lineEnd=Point (goodLine[2]+200*goodLine[0],goodLine[3]+200*goodLine[1]);
    for (i=0;i<mask.rows;i++){
        for (j=0;j<mask.cols;j++){
            if (mask.at<unsigned char> (i,j) > 0 ){
                a2=(-goodLine[3]+j)/(-goodLine[2]+i);
                if (a1*a2 > 0)
                    theta=abs(abs(atan(a1))+abs(atan(a2)));
                else
                    theta=abs(abs(atan(a1))-abs(atan(a2)));
                temp=twoPointDst(Point(i,j),Point(goodLine[2],goodLine[3]));

                temp = ((i - lineStart.x) * (lineEnd.y - lineStart.y) - (j - lineStart.y) * (lineEnd.x - lineStart.x)) / twoPointDst (Point (i,j),Point(goodLine[2],goodLine[3]));

                if (temp> width*4)
                    mask.at<unsigned char> (i,j)=0;
            }
        }
    }
    return goodLine;
}

class controller {
public:
    vector <Mat> stored_images;
    vector <Mat> stored_descriptors;
    vector <vector<KeyPoint> > stored_keyPoints;
    vector <float> line_displacement;
    vector <float> middle_of_line_position;
    vector <KeyPoint> prevKeypoints;
    Mat prevDescriptor;
    Mat latest_valid_image;
    Mat latest_valid_ROI;
    bool is_position_reliable;
    float horizontal_speed;
    int dir;//right=1	left=-1
    int insertCurrentImage(Mat current_image);
};

int controller::insertCurrentImage(Mat current_image){
    Point2f line_y(0,0);
    vector <float> displacement(3);
    int i,j,counter;
    is_position_reliable=0;
    inspecObj ins(current_image);
    if (latest_valid_ROI.empty()){
        latest_valid_ROI=Mat::ones(current_image.size(),CV_8UC1);
    }
    ins.prevDescriptor=prevDescriptor;
    ins.prevKeypoints=prevKeypoints;
    ins.setParams(CAMERA_APERTURE,INSPECTING_LINE_WIDTH);
    ins.preProcess();
    cout << "preProcess\n";
    if (ins.processing().empty()){
        cout << "ins.processing is empty\n" ;
        return -1;
    }
    if (ins.extraProccess()==-1){
//        cout << "going to extraProccess again\n";
        ins.ROI=latest_valid_ROI.clone();
        ins.originalImg=latest_valid_image.clone();
        ins.extraProccess();
    }
    latest_valid_ROI=ins.valid_ROI.clone();  
    latest_valid_image=ins.valid_image.clone();

    if (!ins.keyPoints.empty())
    {
        prevKeypoints=ins.prevKeypoints;
        prevDescriptor=ins.prevDescriptor;
    }
  //  cout << "extra process\n";
    if (ins.descriptor.empty() && stored_images.size() > IMAGES_TAKEN_COUNT){
//        cout << "prev matches!\n";
        ins.descriptor=stored_descriptors[IMAGES_TAKEN_COUNT-1];
        ins.keyPoints=stored_keyPoints[IMAGES_TAKEN_COUNT-1];
    }
    if (stored_images.size() < IMAGES_TAKEN_COUNT){	//filling train data
        if (ins.descriptor.empty()){
            cout << "a train image has rejected\n";
            return -1;
        }
        stored_images.push_back(current_image);
        stored_descriptors.push_back(ins.descriptor);
        stored_keyPoints.push_back(ins.keyPoints);
        //finding center of the Line
        counter=0;
        for (i=0;i<ins.ROI.rows;i++){
            for (j=0;j<ins.ROI.cols;j++){
                if (ins.ROI.at<unsigned char> (i,j)){
                    line_y+=Point2f(i,j);
                    counter++;
                }
            }
        }
        line_y.x=line_y.x/counter;
        line_y.y=line_y.y/counter;
        middle_of_line_position.clear();
        middle_of_line_position.push_back(line_y.y);
        middle_of_line_position.push_back(line_y.x);
        middle_of_line_position.push_back(ins.distance);
        return 1;
    }

    Hinspect Hins(stored_descriptors,stored_keyPoints);
//    cout << "Hins\n" ;
    Hins.match(ins.descriptor);
//    cout << "match\n";
    Hins.rebuildMask(ins.ROI,INSPECTING_LINE_WIDTH,ins.keyPoints);
 //   cout << "rebuild\n";
    //displacement=ins.calcDisplacement(stored_images[IMAGES_TAKEN_COUNT-1],ins.keyPoints,middle_of_line_position[2]);
    imshow("Final Image",ins.ROI);
    //finding center of the Line
    counter=0;

    for (i=0;i<ins.ROI.rows;i++){
        for (j=0;j<ins.ROI.cols;j++){
            if (ins.ROI.at<unsigned char> (i,j)){
                line_y+=Point2f(i,j);
                counter++;
            }
        }
    }

    line_y.x=line_y.x/counter;
    line_y.y=line_y.y/counter;
    if (counter==0){
        line_y.x=middle_of_line_position[1] ;
        line_y.y=middle_of_line_position[0] ;
        cout << "ins.ROI isn't acceptable\n" ;
    }
    middle_of_line_position.clear();
    middle_of_line_position.push_back(line_y.y);
    middle_of_line_position.push_back(line_y.x);
    middle_of_line_position.push_back(ins.distance);

    stored_images.erase(stored_images.begin());
    stored_images.push_back(current_image);
    stored_descriptors.erase(stored_descriptors.begin());
    stored_descriptors.push_back(ins.descriptor);
    stored_keyPoints.erase(stored_keyPoints.begin());
    stored_keyPoints.push_back(ins.keyPoints);
    is_position_reliable=1;
    cout << "middle of  the line=" << "	  x="<<middle_of_line_position[0]<< "	y="<< middle_of_line_position[1] <<"	z="<< middle_of_line_position[2]<< endl;
    return 0;
}

int run_counter=0;
controller Controller;
int image_process_done = 1;
ros::Publisher pub;
void finderMain(const sensor_msgs::Image::ConstPtr& msg)
{
	cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }
    Mat input_image = Mat(cv_ptr->image);
	imshow("Input Image",input_image);

	waitKey(3);
	if (image_process_done){
		image_process_done = 0;
		if (Controller.insertCurrentImage(input_image) == -1)
            return;
		std_msgs::String output;
		stringstream ss;
		ss << "{\'measure_Yi\': "<< Controller.middle_of_line_position[0] << ", \'measure_Xi\': " <<  Controller.middle_of_line_position[1] << ", \'measure_Zi\': " << Controller.middle_of_line_position[2] << "}"; 
		output.data = ss.str();
		printf(" out = %s\n",output.data.c_str());
		
		pub.publish(output); 
		image_process_done = 1;
	}
}


int main(int argc, char **argv)
{
  namedWindow("Input Image",CV_WINDOW_NORMAL);
  namedWindow("Final Image",CV_WINDOW_NORMAL);

  ros::init(argc, argv, "line_finder");

  ros::NodeHandle n;
  string image_topic_name = "/ardrone/image_raw";
  String observer_topics_name = "ros_control/vision_out_receiver";
  ros::Subscriber sub = n.subscribe(image_topic_name, 1, finderMain);
  pub = n.advertise <std_msgs::String>(observer_topics_name,1);
  ros::spin();

  return 0;
}