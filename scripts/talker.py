#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from vrep_test.msg import t1
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
import numpy as np

def talker():
	rospy.init_node('talker')
	pub = rospy.Publisher('mytopic', String,queue_size=10)
	r = rospy.Rate(10)
	while not rospy.is_shutdown():

		print pub.publish("sepehr")
		r.sleep()
		

if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException: pass