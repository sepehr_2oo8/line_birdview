#!/usr/bin/env python
import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class image_converter:

  def __init__(self):
    self.image_pub = rospy.Publisher("/ardrone/image_raw",Image)

    cv2.namedWindow("Image window 1", cv2.WINDOW_NORMAL)
    cv2.namedWindow("Image window 2", cv2.WINDOW_NORMAL)
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("myRosBubbleRob/quad_front_vision",Image,self.callback)
    self.image_sub = rospy.Subscriber("myRosBubbleRob/quad_floor_vision",Image,self.callback2)
  def callback(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "passthrough")
    except CvBridgeError, e:
      print e

    (rows,cols,channels) = cv_image.shape
    #if cols > 60 and rows > 60 :
    #  cv2.circle(cv_image, (50,50), 10, 255)

    cv2.imshow("Image window 1", cv_image)
    cv2.waitKey(3)

    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    except CvBridgeError, e:
      print e

  def callback2(self,data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "passthrough")
    except CvBridgeError, e:
      print e

    (rows,cols,channels) = cv_image.shape
    #if cols > 60 and rows > 60 :
    #  cv2.circle(cv_image, (50,50), 10, 255)

    cv2.imshow("Image window 2", cv_image)
    cv2.waitKey(3)

    try:
      self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    except CvBridgeError, e:
      print e
def main(args):
  rospy.init_node('image_converter', anonymous=True)
  bridge = CvBridge()
  image_pub = rospy.Publisher("/ardrone/image_raw",Image)
  cv_image = cv2.imread("/home/sepehr/Untitled.jpg")
  image_pub.publish(bridge.cv2_to_imgmsg(cv_image, "bgr8"))
  rospy.sleep(1)
  i = 0
  while i < 40:
    image_pub.publish(bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    rospy.sleep(1)
    print i
    i = i+1
  rospy.sleep(20)
  rospy.spin()

  cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)