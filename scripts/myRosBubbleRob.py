#!/usr/bin/env python

import rospy
import re
from vrep_common.srv import simRosAddStatusbarMessage
from vrep_common.srv import simRosEnableSubscriber
from vrep_common.srv import simRosGetObjectHandle
#from vrep_common.srv import simRosGetVisionSensorImage
from vrep_common.srv import simRosEnablePublisher
from std_msgs.msg import String 

from enum import Enum 
class vrep_params(Enum):
	simros_strmcmdnull_start = 0

	#/* from here on, commands are only identified by their code */
	simros_strmcmd_get_object_selection = 1
	simros_strmcmd_get_info = 2 #// do not use. Is streamed anyway with topic name "info"

	simros_strmcmdnull_subscriber_start=0x000800 
	simros_strmcmd_add_status_bar_message=0x000801 
	simros_strmcmd_set_object_selection=0x000802 
	simros_strmcmd_set_joint_state=0x000803 

	simros_strmcmdint_start			=0x001000 
	#/* from here on, commands are also identified by 1 additional int */
	simros_strmcmd_get_array_parameter=0x001001
	simros_strmcmd_get_boolean_parameter=0x001002
	simros_strmcmd_get_dialog_result = 0x001003
	simros_strmcmd_get_floating_parameter = 0x001004
	simros_strmcmd_get_integer_parameter = 0x001005
	simros_strmcmd_get_joint_state  = 0x001006
	simros_strmcmd_get_object_parent  = 0x001007
	simros_strmcmd_get_objects = 0x001008
	simros_strmcmd_get_string_parameter  = 0x001009
	simros_strmcmd_get_ui_event_button = 0x00100A
	simros_strmcmd_get_vision_sensor_depth_buffer = 0x00100B
	simros_strmcmd_get_vision_sensor_image = 0x00100C
	simros_strmcmd_read_collision = 0x00100D
	simros_strmcmd_read_distance = 0x00100E
	simros_strmcmd_read_force_sensor = 0x00100F
	simros_strmcmd_read_proximity_sensor = 0x001010
	simros_strmcmd_read_vision_sensor = 0x001011
	simros_strmcmd_get_vision_sensor_info = 0x001012
	simros_strmcmd_get_range_finder_data = 0x001013
	simros_strmcmd_get_laser_scanner_data = 0x001014
	simros_strmcmd_get_odom_data = 0x001015
	simros_strmcmd_get_depth_sensor_data = 0x001016

	simros_strmcmdint_subscriber_start = 0x001800 
	simros_strmcmd_auxiliary_console_print = 0x001801
	simros_strmcmd_set_array_parameter = 0x001802
	simros_strmcmd_set_boolean_parameter = 0x001803
	simros_strmcmd_set_floating_parameter = 0x001804
	simros_strmcmd_set_integer_parameter = 0x001805
	simros_strmcmd_set_joint_force = 0x001806
	simros_strmcmd_set_joint_position = 0x001807
	simros_strmcmd_set_joint_target_position = 0x001808
	simros_strmcmd_set_joint_target_velocity = 0x001809
	simros_strmcmd_set_vision_sensor_image = 0x00180A
	simros_strmcmd_set_joy_sensor = 0x00180B
	simros_strmcmd_set_twist_command = 0x00180C
	
	simros_strmcmdintint_start = 0x002000 
	#/* from here on, commands are also identified by 2 additional ints */
	simros_strmcmd_get_object_pose = 0x002001
	simros_strmcmd_get_object_float_parameter = 0x002002
	simros_strmcmd_get_object_int_parameter = 0x002003
	simros_strmcmd_get_ui_button_property = 0x002004
	simros_strmcmd_get_ui_slider = 0x002005
	simros_strmcmd_get_transform = 0x002006
	simros_strmcmd_get_object_group_data = 0x002007

	simros_strmcmdintint_subscriber_start = 0x002800 
	simros_strmcmd_set_object_float_parameter = 0x002801
	simros_strmcmd_set_object_int_parameter = 0x002802
	simros_strmcmd_set_object_pose = 0x002803
	simros_strmcmd_set_object_position = 0x002804
	simros_strmcmd_set_object_quaternion = 0x002805
	simros_strmcmd_set_ui_button_label = 0x002806
	simros_strmcmd_set_ui_button_property = 0x002807
	simros_strmcmd_set_ui_slider = 0x002808


	simros_strmcmdstring_start = 0x003000
	#/* from here on, commands are also identified by one additional string */
	simros_strmcmd_get_float_signal = 0x003001
	simros_strmcmd_get_integer_signal = 0x003002
	simros_strmcmd_get_string_signal = 0x003003
	simros_strmcmd_reserved1 = 0x003004
	simros_strmcmd_get_and_clear_string_signal = 0x003005

	simros_strmcmdstring_subscriber_start = 0x003800 
	simros_strmcmd_clear_float_signal = 0x003801
	simros_strmcmd_clear_integer_signal = 0x003802
	simros_strmcmd_clear_string_signal = 0x003803
	simros_strmcmd_set_float_signal = 0x003804
	simros_strmcmd_set_integer_signal = 0x003805
	simros_strmcmd_set_string_signal = 0x003806
	simros_strmcmd_reserved2 = 0x003807
	simros_strmcmd_append_string_signal = 0x003808
	simros_strmcmd_set_joint_trajectory = 0x003809

	simros_strmcmdintstring_start = 0x004000
	#/* from here on, commands are also identified by one additional int and one additional string */
	simros_strmcmd_get_twist_status = 0x004001
	simros_strmcmdintstring_subscriber_start = 0x004800 
	simros_strmcmdreserved_start = 0x005000

	sim_handle_all = -2
	sim_handle_all_except_explicit = -3
	sim_handle_self = -4
	sim_handle_main_script = -5
	sim_handle_tree	= -6
	sim_handle_chain = -7
	sim_handle_single = -8
	sim_handle_default = -9
	sim_handle_all_except_self = -10
	sim_handle_parent = -11
	sim_handle_scene = -12


def main():
	#Initialization
	rospy.init_node('myRosBubbleRob')
	rospy.loginfo("myRosBubbleRob has started")
	
	#Add a status bar message using ros service
	rospy.wait_for_service('/vrep/simRosAddStatusbarMessage')
   	#rospy.loginfo("service is there")
 	statMsg = rospy.ServiceProxy('/vrep/simRosAddStatusbarMessage',simRosAddStatusbarMessage)
	print statMsg("ROS Quadricopter script has launched")
	
	#enabling a subscriber on quad target and also "remoteSubEn"
	rospy.wait_for_service('/vrep/simRosGetObjectHandle')
	getHandle = rospy.ServiceProxy('/vrep/simRosGetObjectHandle',simRosGetObjectHandle)
	Handle = getHandle('Quadricopter_target')
	print Handle
	Quad_Handle = getHandle('Quadricopter')
	print Quad_Handle

	if str(Handle) != "handle: -1":
		target_handle = int (re.match(r"\w+\: (?P<ID>\d+)",str(Handle)).group('ID'))
		quad_handle = int (re.match(r"\w+\: (?P<ID>\d+)",str(Quad_Handle)).group('ID'))
		
		rospy.wait_for_service('/vrep/simRosEnableSubscriber')
		remoteSubEn = rospy.ServiceProxy('/vrep/simRosEnableSubscriber',simRosEnableSubscriber)
		print remoteSubEn("/"+rospy.get_name()+"/quad_target",1,vrep_params.simros_strmcmd_set_object_position.value,target_handle,quad_handle,'')
	else:
		rospy.loginfo("there is no Quadricopter_target object in the scene")

	#enabling a publisher on vision sensor and also "remotePubEn"
	getHandle = rospy.ServiceProxy('/vrep/simRosGetObjectHandle',simRosGetObjectHandle)
	Handle = getHandle('Quadricopter_frontVision');
	if str(Handle) != "handle: -1":
		front_vision_handle = int (re.match(r"\w+\: (?P<ID>\d+)",str(Handle)).group('ID'))
		print "front_vision_handle = 	" + repr(front_vision_handle)
		rospy.wait_for_service('/vrep/simRosEnablePublisher')
		remotePubEn = rospy.ServiceProxy('/vrep/simRosEnablePublisher',simRosEnablePublisher)
		print remotePubEn(rospy.get_name()+"/quad_front_vision",1,vrep_params.simros_strmcmd_get_vision_sensor_image.value,front_vision_handle,-1,'')
	else:
		rospy.loginfo("there is no Quadricopter_frontVision object in the scene")

	#enabling publisher for floor vision sensor
	Handle = getHandle('Quadricopter_floorVision');
	if str(Handle) != "handle: -1":
		floor_vision_handle = int (re.match(r"\w+\: (?P<ID>\d+)",str(Handle)).group('ID'))
		print "floor_vision_handle = 	" + repr(floor_vision_handle)
		print remotePubEn(rospy.get_name()+"/quad_floor_vision",1,vrep_params.simros_strmcmd_get_vision_sensor_image.value,floor_vision_handle,-1,'')
	else:
		rospy.loginfo("there is no Quadricopter_floorVision object in the scene")

	#enabling publisher for quad pose 
	Handle = getHandle('Quadricopter');
	if str(Handle) != "handle: -1":
		Quadricopter_handle = int (re.match(r"\w+\: (?P<ID>\d+)",str(Handle)).group('ID'))
		print "Quadricopter_handle = 	" + repr(Quadricopter_handle)
		print remotePubEn(rospy.get_name()+"/Quadricopter_pose",1,vrep_params.simros_strmcmd_get_object_pose.value,Quadricopter_handle,-1,'')
	else:
		rospy.loginfo("there is no Quadricopter object in the scene")


	rospy.spin()

if __name__ == '__main__':
	main()