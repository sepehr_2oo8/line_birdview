from ardrone_autonomy.msg import Navdata
import  rospy
import cv2
import ast
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class Feedback(object):
    def __init__(self, outputs_type = "ardrone_manual",outputs_dict = -1, vision_feedback1 = False, vision_feedback2 = False):
        self.outputs_type = outputs_type
        print "\n ------- Feedback Configuration --------\n"
        # General Configuration
        self.feedback_pub = rospy.Publisher("ros_control/feedback", String, queue_size = 10)


        # ardrone feedback
        if (self.outputs_type == "ardrone_manual") | (self.outputs_type == "ardrone_auto") | (self.outputs_type == "ardrone_auto_2"):
            self.feedback_is_set = True
            self.outputs_dict = {
                "rotX" : 0,
                "rotY" : 0,
                "rotZ" : 0,
                "magX" : 0,
                "magY" : 0,
                "magZ" : 0,
                "aX" : 0,
                "aY" : 0,
                "aZ" : 0,
                "altd" : 0,
                "vX" : 0,
                "vY" : 0,
                "vZ" : 0,
                "battery" : 0,
                "pressure" : 0
                # There is more
            }
            self.navdata_sub=rospy.Subscriber("ardrone/navdata",Navdata,self.navData)

            print "Available feebacks are \n" + str(self.outputs_dict.keys())
            return

        #vrep quad feedback
        if (self.outputs_type == 'Quadricopter') | (self.outputs_type == 'Quadricopter_manual'):
            print "Quadricopter"
            cv2.namedWindow("Front_Camera", cv2.WINDOW_NORMAL)
            cv2.namedWindow("Floor_Camera", cv2.WINDOW_NORMAL)
            self.bridge = CvBridge()
            self.image_sub = rospy.Subscriber("PowerLineVer2/quad_front_vision",Image,self.front_camera_callback)
            self.image_sub = rospy.Subscriber("PowerLineVer2/quad_floor_vision",Image,self.floor_camera_callback)
            
            self.feedback_is_set = True
            self.outputs_dict = {"qX" : 0,"qY" : 0,"qZ" : 0,"aX" : 0,"aY" : 0,"aZ" : 0}
            print "Available feebacks are \n" + str(self.outputs_dict)
            self.Quadricopter_gyro_sub = rospy.Subscriber("vrep/Gyro", String, self.updateFeedback)
            self.Quadricopter_accel_sub = rospy.Subscriber("vrep/Accelerometer", String, self.updateFeedback)
            return

        self.outputs_dict = outputs_dict
        if type(outputs_dict) is not dict:
            self.feedback_is_set = False
            print "configure feedback first"

    def run(self):
        msg_out = str(self.outputs_dict)
        self.feedback_pub.publish(msg_out)
        return

    def updateFeedback(self,data):
        if (self.outputs_type == 'Quadricopter') | (self.outputs_type == 'Quadricopter_manual'):
            Dict = ast.literal_eval(data.data)
            for k in Dict.keys():
                self.outputs_dict[k] = Dict[k]

    def front_camera_callback(self,data):
        try:
          cv_image = self.bridge.imgmsg_to_cv2(data, "passthrough")
        except CvBridgeError, e:
          print e

        cv2.imshow("Front_Camera", cv_image)
        cv2.waitKey(10)

    def floor_camera_callback(self,data):
        try:
          cv_image = self.bridge.imgmsg_to_cv2(data, "passthrough")
        except CvBridgeError, e:
          print e

        cv2.imshow("Floor_Camera", cv_image)
        cv2.waitKey(10)

    def configure(self,node_is_init = False):
        if node_is_init is False:
            print "initialize the node first"
            return -1
        if (self.outputs_type is "ardrone_navdata"):
            rospy.Subscriber("ardrone/navdata",Navdata,self.navData)

    # Declare all the feedback functions here
    def navData(self,data):
        #print 'NAVDATA'
        self.outputs_dict["rotX"] = data.rotY
        self.outputs_dict["rotY"] = data.rotX
        self.outputs_dict["rotZ"] = data.rotZ
        self.outputs_dict["magX"] = -data.magY
        self.outputs_dict["magY"] = data.magX
        self.outputs_dict["magZ"] = data.magZ
        self.outputs_dict["aX"] = -data.ay
        self.outputs_dict["aY"] = data.ax
        self.outputs_dict["aZ"] = data.az
        self.outputs_dict["vX"] = -data.vy
        self.outputs_dict["vY"] = data.vx
        self.outputs_dict["vZ"] = data.vz
        self.outputs_dict["altd"] = data.altd
        self.outputs_dict["battery"] = data.batteryPercent
        self.outputs_dict["pressure"] = data.pressure
        #rospy.loginfo("feedback list has updated")




