import ControlSystem

def Log_Controller(Input):
	ControlSystem.Logger.log(Input)

def Log_Observer(Xes,Xg,meas,X,TiME):

	Dic_Open = {
	    'Xop' : Xes[0],
	    'VXop' : Xes[1],
	    'Yop' : Xes[2],
	    'VYop' : Xes[3],
	    'Zop' : Xes[4],
	    'VZop' : Xes[5],
	    'PHop' : Xes[6],
	    'THop' : Xes[8],
	    'PSop' : Xes[10]
	}
	ControlSystem.Logger.log(Dic_Open)

	Dic_Gyro = {
	    'Xg' : Xg[6],
	    'VXg' : Xg[3],
	    'Yg' : -Xg[7],
	    'VYg' : -Xg[4],
	    'Zg' : Xg[8],
	    'VZg' : Xg[5],
	    'PHg' : Xg[0],
	    'THg' : Xg[1],
	    'PSg' : Xg[2],
	    'BPHg': Xg[9],
	    'BTHg': Xg[10],
	    'BPSg': Xg[11],
	}
	ControlSystem.Logger.log(Dic_Gyro)

	Dic_Meas = {
	    'Xm' : meas[10],
	    'VXm' : meas[3],
	    'Ym' : meas[11],
	    'VYm' : meas[4],
	    'Zm' : meas[12],
	    'VZm' : meas[5],
	    'PHm' : meas[0],
	    'THm' : meas[1],
	    'PSm' : meas[2],
	    'BPHm' : meas[6],
	    'BTHm' : meas[7],
	    'BPSm' : meas[8]
	}
	ControlSystem.Logger.log(Dic_Meas)

	Dic_Close = {
	    'Xcl' : X[0],
	    'VXcl' : X[1],
	    'Ycl' : X[2],
	    'VYcl' : X[3],
	    'Zcl' : X[4],
	    'VZcl' : X[5],
	    'PHcl' : X[6],
	    'THcl' : X[8],
	    'PScl' : X[10]
	}
	ControlSystem.Logger.log(Dic_Close)
	Dic_TIME={
	    'TIME' : TiME
	}
	ControlSystem.Logger.log(Dic_TIME)