import numpy as np
import math
from math import sin, cos
from filterpy.kalman import UnscentedKalmanFilter as UKF
from filterpy.common import Q_discrete_white_noise
from numpy.random import randn
import ControlSystem
import Log

g = 9.8100
L=0.1800
m=0.200
mu=0.5600
pi= math.pi
def state_estimate_Gyro(Xg, feedback_in, dt):
    T=m*g*math.cos(Xg[0])*math.cos(Xg[1])
    Ug = np.zeros(6)
    Ug[0] = feedback_in['qX']
    Ug[1] = feedback_in['qY']
    Ug[2] = feedback_in['qZ']
    Ug[3] = feedback_in['aX']
    Ug[4] = feedback_in['aY']
    Ug[5] = feedback_in['aZ']+g
    #-----------------------------------------------------------------
    dXg = np.zeros(len(Xg))#not True
    dXg[0] = Ug[0]-Xg[9]+(Ug[1]-Xg[10])*(math.sin(Xg[0])*math.tan(Xg[1]))+Ug[2]*(math.cos(Xg[0])*math.tan(Xg[1]))
    dXg[1] = (Ug[1]-Xg[10])*math.cos(Xg[0])-Ug[2]*math.sin(Xg[0])
    dXg[2] = 0#(Ug[1]-Xg[9])*math.sin(Xg[0])/math.cos(Xg[1])+Ug[2]*math.cos(Xg[0])/math.cos(Xg[1])
    dXg[3] = -g*math.sin(Xg[1])-mu/m*Xg[3]
    dXg[4] = -g*math.cos(Xg[1])*math.sin(Xg[0])-mu/m*Xg[4]
    dXg[5] = -10*Xg[5]+Ug[5]
    dXg[6] = Xg[3]
    dXg[7] = Xg[4]
    dXg[8] = Xg[5]
    dXg[9] = -2*Xg[9]
    dXg[10] = -2*Xg[10]
    dXg[11] = -2*Xg[11]

    Xg += dXg * dt
    return Xg

def state_estimate_Gyro_Drone(Xg, feedback_in, dt,TiME):
    T=m*g*math.cos(Xg[0])*math.cos(Xg[1])
    Ug = np.zeros(6)
    Ug[0] = feedback_in['rotX']*pi/180
    Ug[1] = feedback_in['rotY']*pi/180
    Ug[2] = feedback_in['rotZ']*pi/180
    Ug[3] = feedback_in['vX']/1000
    Ug[4] = feedback_in['vY']/1000
    Ug[5] = feedback_in['vZ']/1000
    #-----------------------------------------------------------------
    dXg = np.zeros(len(Xg))#not True
    #dXg[0] = 0
    #dXg[1] = 0
    #dXg[2] = 0
    dXg[3] = -g*math.sin(Ug[1])-mu/m*Xg[3]
    dXg[4] = -g*math.cos(Ug[1])*math.sin(Ug[0])-mu/m*Xg[4]
    dXg[5] = Ug[5]
    dXg[6] = Xg[3]
    dXg[7] = Xg[4]
    dXg[8] = Xg[5]
    dXg[9] = 0
    dXg[10] = 0
    dXg[11] = 0

    Xg += dXg * dt
    Xg[0] = Ug[0]-Xg[9]
    Xg[1] = Ug[1]-Xg[10]
    Xg[2] = Ug[2]-Xg[11]

    return Xg

def initialized_Gyro_Observer():
      X = np.zeros(12)
      X[8]= 0.5
      return  X

def initialized_Measurement():
      X = np.zeros(16)
      X[9]=1
      X[12]=0.5
      return  X

def initialized_Model_Observer():
      X = np.zeros(12)
      X[4]= 0.5
      return  X

def prediction_Model_Observer(X,control_in, dt):
    S = np.zeros(4)
    #print "control_in = ", control_in
    try:
        S[0] = 4*control_in['X']#+0.0080
        S[1] = 4*control_in['Y']#+0.0580
        S[2] =1.5*control_in['Yaw']#+0.0000
        S[3] = 0.5*control_in['Z']#+0.0000
    except:
        S[0] = 4*control_in['vel_X']#+0.0080
        S[1] = 4*control_in['vel_Y']#+0.0580
        S[2] =1.5*control_in['vel_Yaw']#+0.0000
        S[3] = 0.5*control_in['vel_Z']#+0.0000

    U = np.zeros(4)
    U[0]= g/(math.cos(X[3])*math.cos(X[5]))
    U[1]=-30*X[3]-18*X[4]-2.4*X[1]+S[1]
    U[2]=-30*X[5]-18*X[6]-2.4*X[0]+S[0]
    U[3]= 0
    #-----------------------------------------------------------------
    dX = np.zeros(len(X))
    #dX[0]=0.526*X[1] #0: x, 1: vX, 2: y, 3: vY, 4: z, 5: vZ, 6: phi, 7:phi_dot, 8:theta, 9:theta_dot, 10:psi, 11:psi_dot
    dX[0]=U[0]*(math.cos(X[3])*math.sin(X[5])*math.cos(X[7])+math.sin(X[3])*math.sin(X[7])) 
    #dX[2]=0.526*X[3]
    dX[1]=-U[0]*(math.cos(X[3])*math.sin(X[5])*math.sin(X[7])-math.sin(X[3])*math.cos(X[7]))
    
   # print "X[5] = ",X[5],"S[3] = ", S[3]
    #dX[4]=X[5]
    dX[2]= -X[2]+S[3]
    dX[3]=X[4]
    dX[4]=U[1]*L
    dX[5]=X[6]
    dX[6]=U[2]*L
    dX[7]=X[8]
    #print "X[11] = ",X[11],"S[2] = ", S[2]
    dX[8]=-X[8]+S[2]
    X += dX * dt
    if X[7] > math.pi:
        X[7]=X[7]-2*math.pi

    if X[7] < -math.pi:
        X[7]=X[7]+2*math.pi

    return X



# Predicting new middle of line in image space
#--------------------------------------------------------    
vision_control_in = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
def vision_predict(states, dt): # states : point in image 
    control_in = vision_control_in
    states = states.reshape(9,1)
    state = np.zeros(12)
    state[np.array([1,3,5,7,9,11])] = states[[0,1,2,3,4,5]]
    dX = prediction_Model_Observer(state, control_in, dt)
    camera_intrinsic_mat = np.array([701,0 , 400 , 0,789,300 , 0,0,1])
    camera_intrinsic_mat = camera_intrinsic_mat.reshape(3,3)
    
    phys_loc = states[8] * np.dot(np.linalg.inv(camera_intrinsic_mat),states[[6,7,8]])

    quad_rotation_mat = np.array([cos(dX[8])*cos(dX[10]), sin(dX[6])*sin(dX[8])*cos(dX[10])-cos(dX[6])*sin(dX[10]), sin(dX[6])*sin(dX[10])+cos(dX[6])*sin(dX[8])*cos(dX[10]),
                                cos(dX[8])*sin(dX[10]), cos(dX[6])*cos(dX[10])+sin(dX[6])*sin(dX[8])*sin(dX[10]), cos(dX[6])*sin(dX[8])*sin(dX[10])-sin(dX[6])*cos(dX[10]),
                                -sin(dX[8]), sin(dX[6])*cos(dX[8]), cos(dX[6])*cos(dX[8])] )
    quad_rotation_mat = quad_rotation_mat.reshape(3,3)

    phys_loc = np.dot(np.linalg.inv(quad_rotation_mat),phys_loc)

    phys_loc = phys_loc - np.array([[dX[0]],[dX[2]],[dX[4]]])
    if abs(states[8]) > 0.1:
        predicted_middle_of_line = np.dot(camera_intrinsic_mat,(phys_loc/states[8]))
    else:
        predicted_middle_of_line = np.dot(camera_intrinsic_mat,(phys_loc/0.1))

    predicted_middle_of_line[2] = states[8]
    predicted_middle_of_line = predicted_middle_of_line.reshape(3,)

    states = states.reshape(9,)

    states[range(6)] = dX[[1,3,5,7,9,11]]
    states[range(6,9)] = predicted_middle_of_line[range(3)]
    states = states.reshape(9,)
    return states
#--------------------------------------------------------

# Measurment function
#--------------------------------------------------------   
def vision_measuement(states):
    
    return states[[6,7,8]]
#--------------------------------------------------------
VISION_OBSERVER_2_BIAS = np.array([85, 156, 2, math.pi/2])
def vision_UKF_observer(measurement_from_line, dt):
    if "ukf" not in locals():
        ukf = UKF(dim_x=9, dipredicted_middle_of_linem_z=3, dt=1, hx=vision_measuement, fx=vision_predict, kappa=2.)
        ukf.x = np.zeros(9)

        ukf.Q = np.eye(9)*5000

        ukf.R = np.eye(3)*0.1+np.reshape(np.ones(9),[3,3])*0.05
        ukf.P *= 1000

    ukf._dt = dt
    ukf.predict()
    z = np.array([measurement_from_line['measure_Xi'],measurement_from_line['measure_Yi'],measurement_from_line['measure_Zi']])

    ukf.update(z)

    out = {
        'Xi' : ukf.x[6],
        'Yi' : ukf.x[7],
        'Zi' : ukf.x[8]
    }

    return out


def vision_predict_2(states, dt):

    control_in = vision_control_in 
    states = states.reshape(13,1)
    state = np.zeros(9)
    state[range(9)] = states[range(9)]
    dX = prediction_Model_Observer(state, control_in, dt)
    camera_intrinsic_mat = np.array([316.5,0 , 85.5 , 0,316.5,156.0 , 0,0,1])
    camera_intrinsic_mat = camera_intrinsic_mat.reshape(3,3)
    
    #states[[12,13]] =  np.array([300 + np.random.rand(1)*10,400+np.random.rand(1)*10])
    #states[14] = 2+ np.random.rand(1) * 0.2
    #print "states[12,13,14] = " , states[[12,13,14]]
    phys_loc = np.dot(np.linalg.inv(camera_intrinsic_mat),states[[9,10,11]])
    
    #print "phys_loc 1", phys_loc
    #quad_rotation_mat = np.array([cos(dX[8])*cos(dX[10]), sin(dX[6])*sin(dX[8])*cos(dX[10])-cos(dX[6])*sin(dX[10]), sin(dX[6])*sin(dX[10])+cos(dX[6])*sin(dX[8])*cos(dX[10]),
    #                            cos(dX[8])*sin(dX[10]), cos(dX[6])*cos(dX[10])+sin(dX[6])*sin(dX[8])*sin(dX[10]), cos(dX[6])*sin(dX[8])*sin(dX[10])-sin(dX[6])*cos(dX[10]),
    #                            -sin(dX[8]), sin(dX[6])*cos(dX[8]), cos(dX[6])*cos(dX[8])] )
    #simplified quad rotation matrix, phi and theta = 0
    quad_rotation_mat = np.array([1, 0, 0,
                                0, cos(dX[3]), -sin(dX[3]),
                                0, sin(dX[3]), cos(dX[3])] )
    quad_rotation_mat = quad_rotation_mat.reshape(3,3)
    #print "quad_rotation_mat  =", quad_rotation_mat
    phys_loc = np.dot(np.linalg.inv(quad_rotation_mat),phys_loc)
    #print "displacement = ", dt, np.array([[dX[1]],[dX[3]],[dX[5]]])
    phys_loc = phys_loc - 0.526*dt*np.array([[dX[0]],[dX[1]],[dX[2]]])

    #print "phys_loc =", phys_loc
    distance = phys_loc[2]
    predicted_middle_of_line = np.dot(camera_intrinsic_mat,(phys_loc/phys_loc[2]))
    predicted_middle_of_line[2] = distance

    line_angle = states[12] + dX[3]*dt
    predicted_middle_of_line = predicted_middle_of_line.reshape(3,)


    states = states.reshape(13,)

    states[range(9)] = dX
    states[range(9,12)] = predicted_middle_of_line #np.array([300,400,2])
    #print "predicted_middle_of_line =",predicted_middle_of_line
    states[12] = line_angle
    
    #print states[range(12,16)]
    states = states.reshape(13,)

    return states
#--------------------------------------------------------

# Measurment function
#--------------------------------------------------------   
def vision_measuement_2(states):
    #q = int(states[15]/math.pi)
    #states[15] = states[15] - q*math.pi
    return states[[9,10,11,12]]
#-----------------------------
def vision_UKF_observer_2(observer, measurement_from_line, dt):
    global VISION_OBSERVER_2_BIAS
    ukf = observer
    ukf._dt = dt
    print 'ukf._dt =', ukf._dt
    #print "ukf.x-1 = ", ukf.x
    #print "ukf.P diagonal = ", ukf.P[range(16),range(16)]
    #e_vals, e_vecs = np.linalg.eig(ukf.P)
    #e_vals[e_vals < 1e-10] = 1e-5
    #I = np.eye(e_vals.shape[0])
    #I[I == 1] = e_vals
    #e_vals = I
    #print e_vals.shape
    #print e_vecs.shape
    #ukf.P = np.dot(np.dot(e_vecs,e_vals),np.transpose(e_vecs))
    #print "evals =", e_vals

    ukf.predict()

    print "ukf.xp =", ukf.xp
    #print "ukf.P diagonal after predict = ", ukf.P[range(16),range(16)]
    #print "ukf.x before observation= ", ukf.x
    z = np.array([measurement_from_line['measure_Xm'], measurement_from_line['measure_Ym'],measurement_from_line['measure_Z'], measurement_from_line["measure_theta"]])
    ControlSystem.Logger.log(measurement_from_line)
    #temp z testing 
    noise = np.array([np.random.normal(0,10,1), np.random.normal(0,10,1), np.random.normal(0,0.1,1), np.random.normal(0,0.1,1)])
    #z = np.array([85 ,156, 2, math.pi/2]) + noise.reshape(4,)

    z = z - VISION_OBSERVER_2_BIAS    
    #noise = np.array([np.random.normal(0,100,1), np.random.normal(0,100,1), np.random.normal(0,1,1), np.random.normal(0,1,1)])
    #z = z + noise.reshape(4,)
    print "z = ", z
    ukf.update(z)

    print "ukf.x = ", ukf.x
    #print "ukf.P = ", ukf.P 
    states_to_log = {'vx' : ukf.x[0], 'vy' : ukf.x[1], 'vz' : ukf.x[2], 'phi' : ukf.x[3], 'phi_dot' : ukf.x[4], 'q_theta' : ukf.x[5], 'theta_dot' : ukf.x[6], 'psi' : ukf.x[7], 'psi_dot' : ukf.x[8]} #, 'Xm' : ukf.x[12], 'Ym' : ukf.x[13], 'Zm' : ukf.x[14], 'line_angle' : ukf.x[15] }
    ControlSystem.Logger.log(states_to_log)

    out = {
        'Xm' : ukf.x[9] + VISION_OBSERVER_2_BIAS[0],
        'Ym' : ukf.x[10] + VISION_OBSERVER_2_BIAS[1],
        'Z' : ukf.x[11] + VISION_OBSERVER_2_BIAS[2],
        'theta' : ukf.x[12] + VISION_OBSERVER_2_BIAS[3]
        
    }

    return out

def initObserver(observer_type):
    if observer_type == "vision_UKF_observer_2":
        ukf = UKF(dim_x=13, dim_z=4, dt=0.3, hx=vision_measuement_2, fx=vision_predict_2, kappa=2.)
        #x, vX, y, vY, z, vZ, phi, phi_dot, theta, theta_dot, psi, psi_dot, xi, yi, Z, lines_theta
        ukf.x = np.ones(13)*0.0
        ukf.x[11] = 2
        ukf.x[12] = math.pi/2
        
        #states                      vX,   vY,  vZ,  phi, phi_dot, theta,theta_dot, psi, psi_dot, Xm, Ym, Z, lines_theta
        prediction_vars = np.array([0.05, 0.05, 0.05, 0.05, 0.01,   0.05,  0.01,   0.05,  0.01,   1, 1, 1, 0.5 ])
        ukf.Q = np.zeros([13,13])
        for i in range(13):
            ukf.Q[i,i] = prediction_vars[i]


        ukf.R = np.eye(4)
        ukf.R[0,0] = 1000
        ukf.R[1,1] = 1000
        ukf.R[2,2] = 10
        ukf.R[3,3] = math.pi
        ukf.P *= 1
        P_init = np.array([1, 1, 1, 10,     1,      10,    1,       10,     1,   10000, 10000, 50,   1 ])
        for i in range(13):
            ukf.P[i,i] = P_init[i]   
        return ukf
