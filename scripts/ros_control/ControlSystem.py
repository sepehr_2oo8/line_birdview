import rospy
from Observer import Observer
from Feedback import Feedback
from Transmitter import Transmitter
from Controller import Controller
from Logger import logger 

Logger = logger()
class ControlSystem (object):
    def __init__(self, name = "AnymControlClass", control_system_type = 'Quadricopter', has_observer=True, is_linear=True):
        self.control_system_type = control_system_type
        self.name = name
        
        print "control system initialization: \n"
        self.observer = Observer(control_system_type)
        self.feedback = Feedback(control_system_type)
        self.transmitter = Transmitter(control_system_type)
        self.controller = Controller(control_system_type)


    def run(self,Input):
        Logger.resample()
        self.controller.run(Input)    
        self.transmitter.run(receiver_type = self.control_system_type)
        self.feedback.run()
        self.observer.run()
        rospy.sleep(0.1)
        
    
    def __str__(self):
        print self.name + "it's done"