import rospy
import numpy as np
import math
import time
from std_msgs.msg import String
import ast
import observers_UAV
import Log
import ControlSystem


class Observer(object):
    def __init__(self,observer_type = 'Quadricopter', feedback_in = '', control_in = '', observer_state = ''):
        #General Configuration
        self.observer_type = observer_type
        self.control_out_sub = rospy.Subscriber("ros_control/control_out",String,self.updateControlOut)
        self.feedback_sub = rospy.Subscriber("ros_control/feedback", String, self.updateFeedbackOut)
        self.observer_out_pub = rospy.Publisher("ros_control/observer_out", String, queue_size = 10)
        self.feedback_vector_updated = 0
        self.controller_out_updated = 0
        self.control_in = control_in
        self.observer_state = observer_state
        self.feedback_in = feedback_in
        
        
        self.t = time.time()
        self.TiME=0

        if (observer_type == 'Quadricopter')|(observer_type == 'Quadricopter_manual') :
            self.vision_receiver = rospy.Subscriber("ros_control/vision_out_receiver",String,self.visionReceiver)

            self.feedback_in = {
                'qX' : 0,
                'qY' : 0,
                'qZ' : 0,
                'aX' : 0,
                'aY' : 0,
                'aZ' : 0
            }
            self.control_in = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw': 0
            }
            self.measurement_from_line = {
                'measure_Xm' : 0,
                'measure_Ym' : 0,
                'measure_theta' : 0,
                'measure_Z' : 0
            }
            
            self.observer_state = {
                'Xm' : 0,
                'Ym' : 0,
                'theta' : 0,
                'Z' : 0
            }

            
        if (observer_type == 'ardrone_manual'):
            self.feedback_in = {
                "rotX" : 0,
                "rotY" : 0,
                "rotZ" : 0,
                "magX" : 0,
                "magY" : 0,
                "magZ" : 0,
                "aX" : 0,
                "aY" : 0,
                "aZ" : 0,
                "altd" : 0,
                "vX" : 0,
                "vY" : 0,
                "vZ" : 0,
                "battery" : 0,
                "pressure" : 0
                # There is more
            }

            self.control_in = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.observer_state = {
                'Phi' : 0,
                'Theta' : 0,
                'Psi' : 0,
                'vX' : 0,
                'vY' : 0,
                'vZ' : 0,
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }

        if (observer_type == 'ardrone_auto'):
            self.vision_receiver = rospy.Subscriber("ros_control/vision_out_receiver",String,self.visionReceiver)
            self.measurement_from_line = {
                'measure_Xi' : 0,
                'measure_Yi' : 0,
                'measure_Zi' : 0
            }
            self.feedback_in = {
                "rotX" : 0,
                "rotY" : 0,
                "rotZ" : 0,
                "magX" : 0,
                "magY" : 0,
                "magZ" : 0,
                "aX" : 0,
                "aY" : 0,
                "aZ" : 0,
                "altd" : 0,
                "vX" : 0,
                "vY" : 0,
                "vZ" : 0,
                "battery" : 0,
                "pressure" : 0
                # There is more
            }

            self.control_in = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.observer_state = {
                'Xi' : 0,
                'Yi' : 0,
                'Zi' : 0
            }

        if (observer_type == 'ardrone_auto_2'):
            self.vision_receiver = rospy.Subscriber("ros_control/vision_out_receiver",String,self.visionReceiver)
            self.measurement_from_line = {
                'measure_Xm' : 0,
                'measure_Ym' : 0,
                'measure_theta' : 0,
                'measure_Z' : 0
            }
            self.feedback_in = {
                "rotX" : 0,
                "rotY" : 0,
                "rotZ" : 0,
                "magX" : 0,
                "magY" : 0,
                "magZ" : 0,
                "aX" : 0,
                "aY" : 0,
                "aZ" : 0,
                "altd" : 0,
                "vX" : 0,
                "vY" : 0,
                "vZ" : 0,
                "battery" : 0,
                "pressure" : 0
                # There is more
            }

            self.control_in = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.observer_state = {
                'Xm' : 0,
                'Ym' : 0,
                'theta' : 0,
                'Z' : 0
            }
        self.imp_observer = None
        print "\n---------- Observer Configuration---------"
        print "control input" + str(self.control_in)
        print "feedback input" + str(self.feedback_in)
        print "observer input" + str(self.observer_state)


    def visionReceiver(self,data):
        self.measurement_from_line = ast.literal_eval(data.data)

    def updateControlOut(self,data):
        self.control_in = ast.literal_eval(data.data)

    def updateFeedbackOut(self,data):
        if self.observer_type == "Quadricopter":
            self.feedback_vector_updated = 1
        
        self.feedback_in = ast.literal_eval(data.data)
        #rospy.loginfo( self.feedback_in)

    def run(self):
        if (self.observer_type == 'ardrone_manual'):
            # observer module comes here
            print 'Control in'
            print self.control_in
            dt=(time.time() - self.t)
            g=observers_UAV.g

            SignalControl={
                'X' : self.control_in['vel_X'],
                'Y' : self.control_in['vel_Y'],
                'Z' : self.control_in['vel_Z'],
                'Yaw': self.control_in['vel_Yaw']
            }
            self.Xes=observers_UAV.prediction_Model_Observer(self.Xes,SignalControl,dt)
            self.Xg=observers_UAV.state_estimate_Gyro_Drone(self.Xg,self.feedback_in,dt,self.TiME)
            self.meas=observers_UAV.measurement_Drone(self.meas,self.feedback_in, dt, self.Xes,self.TiME)
            
            if self.TiME>2 and self.TiME<2.2:
                self.meas[13]=self.meas[6]/self.meas[9]
                self.meas[14]=self.meas[7]/self.meas[9]
                self.meas[15]=self.meas[8]/self.meas[9]
                self.Xg[9]=self.meas[13]
                self.Xg[10]=self.meas[14]
                self.Xg[11]=self.meas[15]

            self.X= np.copy(self.Xes)
            
            self.TiME+=dt
            self.t = time.time()
            #Log.Log_Observer(self.Xes,self.Xg,self.meas,self.X,self.TiME)

            self.observer_state['Phi'] = self.X[6]
            self.observer_state['Theta'] = self.X[8]
            self.observer_state['Psi'] = self.X[10]
            self.observer_state['vX'] = self.X[1]
            self.observer_state['vY'] = self.X[3]
            self.observer_state['vZ'] = self.X[5]
            self.observer_state['X'] = self.X[0]
            self.observer_state['Y'] = self.X[2]
            self.observer_state['Z'] = self.X[4]

        if (self.observer_type == 'Quadricopter')|(self.observer_type == 'Quadricopter_manual'):
            if self.imp_observer == None:
                self.imp_observer = observers_UAV.initObserver("vision_UKF_observer_2")
            dt=(time.time() - self.t)
            observers_UAV.vision_control_in = self.control_in
            print "measurement sent to observers_UAV from observer = "
            print self.measurement_from_line
            self.observer_state = observers_UAV.vision_UKF_observer_2(self.imp_observer, self.measurement_from_line,dt)
            ControlSystem.Logger.log(self.observer_state)
            self.t = time.time()

        if (self.observer_type == 'ardrone_auto'):
            print "self.observer_type = ", self.observer_type
            dt=(time.time() - self.t)
            observers_UAV.vision_control_in = self.control_in
            self.observer_state = observers_UAV.vision_UKF_observer(self.measurement_from_line,dt)
            ControlSystem.Logger.log(self.observer_state)
            self.t = time.time()

        if (self.observer_type == 'ardrone_auto_2'):
            print "self.observer_type = ", self.observer_type

            if self.imp_observer == None:
                self.imp_observer = observers_UAV.initObserver("vision_UKF_observer_2")
            dt=(time.time() - self.t)
            observers_UAV.vision_control_in = self.control_in
            print "measurement sent to observers_UAV from observer = "
            print self.measurement_from_line
            self.observer_state = observers_UAV.vision_UKF_observer_2(self.imp_observer, self.measurement_from_line,dt)
            ControlSystem.Logger.log(self.observer_state)
            self.t = time.time()

        #print "self.observer_type = ", self.observer_type

        #print "self.observer_state =", self.observer_state
        msg_out = str(self.observer_state)
        self.observer_out_pub.publish(msg_out)