import rospy
from geometry_msgs.msg import Point
from std_msgs.msg import String
import ast
import tf
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Quaternion

class Transmitter(object):
    def __init__(self,transmitter_type = '', transmitter_in = '', transmitter_out = ''):
        # General Configuration
        self.control_out_sub = rospy.Subscriber("ros_control/control_out", String, self.updateControlOut)
        
        if (transmitter_type == 'Quadricopter') | (transmitter_type == 'Quadricopter_manual'):
            self.data_dict = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            print "\n------ Transmitter Configuration --------"
            print "transmitter data outputs are : " + str(self.data_dict)
            self.out_publisher = rospy.Publisher ('PowerLineVer2/quad_target',PoseStamped,queue_size = 1)
            return

        if (transmitter_type == "ardrone_manual") | (transmitter_type == "ardrone_auto") | (transmitter_type == "ardrone_auto_2"):       
            self.data_dict = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.out_publisher = rospy.Publisher ('cmd_vel',Twist,queue_size = 1)
            print "\n------ Transmitter Configuration --------"
            print "transmitter data outputs are : " + str(self.data_dict)
            return   

        self.input = transmitter_in
        self.output = transmitter_out

    def updateControlOut(self,data):
        self.data_dict = ast.literal_eval(data.data)

    def run(self,receiver_type = 'Quadricopter' ):
        if (receiver_type == 'Quadricopter') | (receiver_type == 'Quadricopter_manual'):
            print "self.data_dict = ", self.data_dict
            p = Point(self.data_dict['vel_X'],self.data_dict['vel_Y'],self.data_dict['vel_Z'])
            q = tf.transformations.quaternion_from_euler(0,0,self.data_dict['vel_Yaw'])
            quaternion = Quaternion(q[0],q[1],q[2],q[3])
            pose = Pose (p,quaternion)
            out = PoseStamped()
            out.pose = pose
            #print "transmitted point is : ", pose
            # publishing is ignored 
            self.out_publisher.publish(out)
            return

        if (receiver_type == 'ardrone_auto') | (receiver_type == 'ardrone_manual') | (receiver_type == 'ardrone_auto_2'):           
            
            linear = Vector3(self.data_dict['vel_Y'],-self.data_dict['vel_X'],self.data_dict['vel_Z'])
            angular = Vector3(0,0,self.data_dict['vel_Yaw'])
            self.out_publisher.publish(linear,angular)
            #print 'values has transmitted to ardrone'
            return 
        
        print "there is no method available for " + receiver_type
        return



