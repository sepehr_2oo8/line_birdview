import rospy
import numpy as np
from std_msgs.msg import String
import ast
import ardrone_autonomy.srv as ardrone_srv
import Log

class Controller(object):
    def __init__(self, controller_type = '', controller_in = '', controller_out = ''):
        # General Configuration
        self.controller_type = controller_type
        self.observer_out_sub = rospy.Subscriber("ros_control/observer_out", String, self.updateObserverOut)
        self.control_out_pub = rospy.Publisher("ros_control/control_out", String,queue_size = 10)

        if controller_type == 'Quadricopter':
            self.observer_in = {
                'Xm' : 0,
                'Ym' : 0,
                'Z' : 0,
                'theta': 0
            }
            self.input= {
                'Xm' : 0,
                'Ym' : 0,
                'Z' : 0,
                'theta': 0
            }
            self.output = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw': 0
            }
            self.controller_imp_type = 'Quadricopter_PID'
            print "\n -------Controller Configuration -------"
            print "controller inputs are : " + str(self.input)
            print "controller outputs are : " + str (self.output)
            self.total_error = np.zeros(len(self.input))
            self.prev_error = np.zeros(len(self.input))
            return
        
        if controller_type == 'ardrone_manual':
            self.input = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.output = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.controller_imp_type = 'ardrone_manual'
            
        if controller_type == 'ardrone_auto':
            self.input = {
                'Xi' : 0,
                'Yi' : 0,
                'Zi' : 0
            }
            self.output = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.observer_in = {
                'Xi' : 0,
                'Yi' : 0,
                'Zi' : 0
            }
            self.controller_imp_type = 'ardrone_auto'
            self.total_error = np.zeros(len(self.input))
            self.prev_error = np.zeros(len(self.input))


        if controller_type == 'ardrone_auto_2':
            self.input = {
                'Xm' : 0,
                'Ym' : 0,
                'Z' : 0,
                'theta' : 0
            }
            self.output = {
                'vel_X' : 0,
                'vel_Y' : 0,
                'vel_Z' : 0,
                'vel_Yaw' : 0
            }
            self.observer_in = {
                'Xm' : 0,
                'Ym' : 0,
                'Z' : 0,
                'theta' : 0
            }
            self.controller_imp_type = 'ardrone_auto_2'
            self.total_error = np.zeros(len(self.input))
            self.prev_error = np.zeros(len(self.input))

        if controller_type == 'Quadricopter_manual':
            self.observer_in = {
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }
            self.input= {
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }
            self.output = {
                'X' : 0,
                'Y' : 0,
                'Z' : 0
            }
            self.controller_imp_type = 'Quadricopter_manual'
            print "\n -------Controller Configuration -------"
            print "controller inputs are : " + str(self.input)
            print "controller outputs are : " + str (self.output)
            return    
        
    def updateObserverOut(self,data):
        #print "data = ", data
        print "self.observer_in in Controller= ", self.observer_in
        self.observer_in = ast.literal_eval(data.data)
        #print "observer input to controller = ",self.observer_in

    def run(self,Input):
        #print "controller run"
        print self.controller_imp_type
        if self.controller_imp_type == 'Quadricopter_PID':
            for w in Input:
                try:
                    self.input[w]
                    self.observer_in[w]
                except:
                    rospy.loginfo("there is no: " + w +"as feedback or input" )
            error = np.zeros(len(self.input))
            #print "controller input is:"
            #print Input
            error_dic = {'Xm': 0 , 'Ym': 0 , 'Z':0, 'theta': 0}
            print "observer_in in controller =", self.observer_in
            print "input in controller = ", Input
            for i in range(len(Input)):
                error_dic[Input.keys()[i]] = Input.values()[i] - self.observer_in[Input.keys()[i]]

            error[0] = error_dic['Xm']
            error[1] = error_dic['Ym']
            error[2] = error_dic['Z']
            error[3] = error_dic['theta']
            
            #print "\nError is:"
            #rospy.loginfo(error)
            Bias = {'Xm': +0.0000 , 'Ym': +0.0000 , 'Z':+0.0000, 'theta': 0}
            #Bias = {'X': -0.0080 , 'Y': -0.0580 , 'Z':-0.0000}
            P = 1.0
            I = 0
            D = 0.1
            #min_thresh = 0.0001

            out = error*P + self.total_error*I + (self.prev_error - error)*D
            self.output['vel_X'] = 0.02#*error_dic['Xm']
            self.output['vel_Y'] = -0.0003*error_dic['Ym'] - 0.0003*(self.prev_error[1] - error_dic["Ym"])
            self.output['vel_Z'] = 0#-0.01*error_dic['Z']
            self.output['vel_Yaw'] = 00.01*error_dic['theta'] + 0.005*(self.prev_error[3] - error_dic["theta"])
            #for i in range(len(out)):
            #    self.output[error_dic.keys()[i]] = P*error_dic.values()[i] + Bias[error_dic.keys()[i]]#self.output[self.output.keys()[i]] = out[i]
            print "Controller output =", self.output
            
            # temp. ignoring the controller
            #self.output = {
            #    'vel_X' : 0,
            #    'vel_Y' : 0,
            #    'vel_Z' : 0,
            #    'vel_Yaw': 0
            #    }
            # -------------
            self.total_error += error
            self.prev_error = error

            
        if (self.controller_imp_type == 'Quadricopter_manual')|(self.controller_imp_type == 'ardrone_manual'):
            self.output = Input
            
        
        if  self.controller_imp_type == 'ardrone_auto':
            error = np.zeros(len(self.input))
            #print "controller input is:"
            #print Input
            error_dic = {'Xi': 0 , 'Yi': 0 , 'Zi':0, 'yaw': 0}
            for i in range(len(Input)):
                error_dic[Input.keys()[i]] = Input.values()[i] - self.observer_in[Input.keys()[i]]

            error[0] = error_dic['Xi']
            error[1] = error_dic['Yi']
            error[2] = error_dic['Zi']

            if self.observer_in.values() == [0,0,0]:
                error = np.zeros(3)

            scale_coeff = np.eye(3)/1600

            P = np.eye(3) * scale_coeff
            D = np.eye(3) * scale_coeff
            I = 0 * scale_coeff

            out = np.dot(error,P) + np.dot(self.total_error,I) + np.dot(self.prev_error - error,D)

            self.output['vel_X'] = 0.02#out[0]
            self.output['vel_Y'] = out[2] * 0.5
            self.output['vel_Z'] = out[0]*2
            self.output['vel_Yaw'] = 0

            self.total_error += error
            self.prev_error = error


        if  self.controller_imp_type == 'ardrone_auto_2':
            for w in Input:
                try:
                    self.input[w]
                    self.observer_in[w]
                except:
                    rospy.loginfo("there is no: " + w +"as feedback or input" )
            error = np.zeros(len(self.input))
            #print "controller input is:"
            #print Input
            error_dic = {'Xm': 0 , 'Ym': 0 , 'Z':0, 'theta': 0}
            print "observer_in in controller =", self.observer_in
            print "input in controller = ", Input
            for i in range(len(Input)):
                error_dic[Input.keys()[i]] = Input.values()[i] - self.observer_in[Input.keys()[i]]

            error[0] = error_dic['Xm']
            error[1] = error_dic['Ym']
            error[2] = error_dic['Z']
            error[3] = error_dic['theta']
            
            print "conrtoller error =", error

            self.output['vel_X'] = 0.001*error_dic['Xm']
            self.output['vel_Y'] = 0.1#0.001*error_dic['Ym']
            self.output['vel_Z'] = 0.1*error_dic['Z']
            self.output['vel_Yaw'] = 0.1*error_dic['theta'] 
            #for i in range(len(out)):
            #    self.output[error_dic.keys()[i]] = P*error_dic.values()[i] + Bias[error_dic.keys()[i]]#self.output[self.output.keys()[i]] = out[i]
            print "Controller output =", self.output
            
            print "self.output (controller output)", self.output
            # temp. ignoring the controller
            #self.output = {
            #    'vel_X' : 0,
            #    'vel_Y' : 0,
            #    'vel_Z' : 0,
            #    'vel_Yaw': 0
            #    }
            # -------------
            self.total_error += error
            self.prev_error = error
        
        Log.Log_Controller(self.output)
        msg_out = str(self.output)
        self.control_out_pub.publish(msg_out)
