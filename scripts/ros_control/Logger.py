from xlwt import Workbook
import os

class logger(object):
    def __init__(self, filename = os.getcwd()+"/log.xls"):#direct User
        self.filename = filename
        self.book = Workbook()
        self.sheet1 = self.book.add_sheet('Sheet 1')
        self.titles = {'sample' : 1}
        self.sheet1.write(0,0,'sample')
        self.sample = 1
        b = self.titles.values()

    def resample(self):
        self.sample += 1

    def log(self, in_dict, names = []):
        if names != []:
            values = in_dict
            in_dict = {}
            for i in range(len(values)):
                in_dict[names[i]] = values[i]
                
        self.titles.keys()
        found_flag = 0
        for keys in in_dict.keys():
            for titles in self.titles.keys():
                if keys == titles:
                    found_flag = 1
            if found_flag:
                self.sheet1.write(self.sample,self.titles[keys], in_dict[keys])
            else:
                self.titles[keys] = len(self.titles.values())
                self.sheet1.write(0,self.titles[keys],keys)
                self.sheet1.write(self.sample,self.titles[keys], in_dict[keys])
            found_flag = 0
        self.book.save(self.filename)



