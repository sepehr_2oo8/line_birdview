#!/usr/bin/env python

import rospy
from vrep_test.srv import testsrv
from vrep_test.msg import t1

def handleFun(data):
	rospy.loginfo("name = %s and ID = %d",data.first_name,data.number)
	return t1(first_name = 'sepehr',number = 123)

def serviceProv():
	rospy.init_node('service_test')
	s = rospy.Service('card_printer', testsrv, handleFun)
	rospy.spin()
if __name__ == '__main__':
	serviceProv();
