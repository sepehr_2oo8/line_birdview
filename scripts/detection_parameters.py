import numpy as np
import math

smoothing_mat_size = (9,9)
smoothing_coeff_1 = 1.5 #sim :1.5 real :7
smoothing_coeff_2 = -0.5 #sim :-0.5 real :-6
steer_sigma =  4# approximate of the seen line_width in pixel
steer_mat_width = steer_sigma*2
steer_mat_spacing = 1
steer_num_angles = 8
f1_thresh_percent_min = 97
f1_thresh_percent_max = 99.5
f2_thresh_percent_min = 95 # sim :changed 95 - real :97
f2_thresh_percent_max = 100 # sim :changed 100 - real :100
dilate_kernel = np.ones((3,3),np.uint8)
houghline_vote_thresh = 20 # sim :20 real :50
houghline_min_length_coeff = 0.8 # sim: 0.8 real : 0.8
houghline_max_gap = 8 # sim :8 real :8

camera_apparatus = math.pi*(60.0/180.0) # camera_apparatus in radians
line_width = 0.03 # line_width in meter