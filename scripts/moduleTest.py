#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from std_msgs.msg import Empty

from vrep_test.msg import t1
from ros_control.ControlSystem import ControlSystem
from geometry_msgs.msg import Point
import cv2
import signal
import sys
import time
import ardrone_autonomy.srv as ardrone_srv
from ardrone_autonomy.srv import LedAnim
from sensor_msgs.msg import Joy
import os
import math


def signal_handler(signal, frame):
	print "shutting down!"
	cv2.destroyAllWindows()
	sys.exit(0)



def main():
	rospy.init_node('moduleTest')

	CS = ControlSystem(control_system_type = 'ardrone_auto_2')

	signal.signal(signal.SIGINT, signal_handler)
	i = 0
	while True:
		try:
			i += 1
			print "---------------- " , i
			Input = {
				'Xm' : 85,
				'Ym' : 120,
				'Z' : 2,
				'theta' : math.pi / 2
			}	
			CS.run(Input)
			rospy.sleep(0.1)
  		except KeyboardInterrupt:
  			print "Shutting down"
  			#os.system("rosnode kill /line_finder")
  			cv2.destroyAllWindows()
  			break


if __name__ == '__main__':
	main()
