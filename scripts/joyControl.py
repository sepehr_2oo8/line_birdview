#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from std_msgs.msg import Empty

from vrep_test.msg import t1
from ros_control.ControlSystem import ControlSystem
from geometry_msgs.msg import Point

from geometry_msgs.msg import Point
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Quaternion

import ardrone_autonomy.srv as ardrone_srv
from ardrone_autonomy.srv import LedAnim
from sensor_msgs.msg import Joy
import os
import tf


Input = {'vel_X' : 0,'vel_Y' : 0,'vel_Z' : 0,'vel_Yaw' : 0}
	

class controller(object):
	def __init__(self, control_type):
		self.control_type = control_type
		rospy.Subscriber('joy',Joy,self.joyStickCallback)

		if self.control_type == "Quadricopter_manual":
			self.out_publisher = rospy.Publisher ('PowerLineVer2/quad_target',PoseStamped,queue_size = 1)
			self.data_dict = {
				'vel_X' : 0,
				'vel_Y' : 0,
				'vel_Z' : 0,
				'vel_Yaw' : 0
			}

		if self.control_type == "ardrone_manual" : 
			self.takeoff_topic = rospy.Publisher("ardrone/takeoff", Empty, queue_size = 1)
			self.landing_topic = rospy.Publisher("ardrone/land", Empty, queue_size = 1)
			self.reset_topic = rospy.Publisher("ardrone/reset", Empty, queue_size = 1)
			self.out_publisher = rospy.Publisher ('cmd_vel',Twist,queue_size = 1)

		if self.control_type == "test_observer":
			self.out_publisher = rospy.Publisher ("ros_control/control_out", String, queue_size = 1)
			self.data_dict = {
				'vel_X' : 0,
				'vel_Y' : 0,
				'vel_Z' : 0,
				'vel_Yaw' : 0
			}
		
		print repr(self.control_type) + "in joyControl has been initialized"
		

	def joyStickCallback(self,data):

		if self.control_type == "ardrone_manual":
			led_srv = rospy.ServiceProxy('ardrone/setledanimation',LedAnim)
			translation_step = 1
			self.data_dict = {
				'vel_X' : -data.axes[0]*translation_step, 
				'vel_Y' : data.axes[1]*translation_step, 
				'vel_Z' : (data.buttons[5]-data.buttons[7])*translation_step, 
				'vel_Yaw' : (data.buttons[4]-data.buttons[6])*translation_step
			}

			if data.axes[0] == 1:
				led_srv(11,4,1)
			if data.axes[0] == -1:
				led_srv(12,4,1)
			if data.axes[1] == 1:
				led_srv(2,4,1)
			if data.axes[1] == -1:
				led_srv(3,4,1)
			if data.buttons[0] == 1:
				self.takeoff_topic.publish()
			if data.buttons[1] == 1:
				self.landing_topic.publish()
			if data.buttons[2] == 1:
				self.reset_topic.publish()

			linear = Vector3(self.data_dict['vel_Y'],-self.data_dict['vel_X'],self.data_dict['vel_Z'])
			angular = Vector3(0,0,self.data_dict['vel_Yaw'])
			self.out_publisher.publish(linear,angular)


		if self.control_type == "Quadricopter_manual":
			translation_step = 0.1
			Input = {
				'vel_X' : data.axes[1]*translation_step, 
				'vel_Y' : data.axes[0]*translation_step, 
				'vel_Z' : (data.buttons[5]-data.buttons[7])*translation_step,
				'vel_Yaw' : (data.buttons[4]-data.buttons[6])*translation_step
			}
			print "joy stick inputs = " + str(Input) 
			p = Point(Input['vel_X'],Input['vel_Y'],Input['vel_Z'])
			q = tf.transformations.quaternion_from_euler(0,0,Input['vel_Yaw'])
			quaternion = Quaternion(q[0],q[1],q[2],q[3])
			pose = Pose (p,quaternion)
			out = PoseStamped()
			out.pose = pose
			print "transmitted point is : ", p
			self.out_publisher.publish(out)

		if self.control_type == "test_observer":
			translation_step = 0.1
			vals = {
				'vel_X' : data.axes[1]*translation_step, 
				'vel_Y' : data.axes[0]*translation_step, 
				'vel_Z' : (data.buttons[5]-data.buttons[7])*translation_step,
				'vel_Yaw' : (data.buttons[4]-data.buttons[6])*translation_step
			}
			out = str(vals)
			self.out_publisher.publish(out)
            
if __name__ == '__main__':
	rospy.init_node('joyControl')
	C = controller("Quadricopter_manual")
	rospy.spin()