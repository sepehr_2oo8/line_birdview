#!/usr/bin/env python
import numpy as np
import cv2
import math
import detection_parameters as dp
import time
import rospy
import ast
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image
from std_msgs.msg import String

sigma = dp.steer_sigma
def G21(x):
    return 0.9213 * (2.0*x*x - 1.0) * math.exp(-x*x/sigma*sigma)
def G22(x):
    return math.exp(-x*x/sigma*sigma)
def G23(x):
    return math.sqrt(1.8430) * x * math.exp(-x*x/sigma*sigma)
def H21(x):
    return 0.9780 * (-2.254 * x + x*x*x) * math.exp(-x*x/sigma*sigma)
def H22(x):
    return math.exp(-x*x/sigma*sigma)
def H23(x):
    return x * math.exp(-x*x/sigma*sigma)
def H24(x):
    return 0.9780 * (-0.7515 + x*x) * math.exp(-x*x/sigma*sigma)

class SteerableG2(object):
    def create(self, width, spacing, func):
        r = np.zeros(width*2+1)
        for i in range(-width,width+1):
            r[i+width] = func(float(i)*spacing)
        return r

    def __init__(self, image, width, sigma):
        self.m_g1 = self.create(width, sigma, G21)
        self.m_g2 = self.create(width, sigma, G22)
        self.m_g3 = self.create(width, sigma, G23)

        #Create separable filters for H2
        self.m_h1 = self.create(width, sigma, H21)
        self.m_h2 = self.create(width, sigma, H22)
        self.m_h3 = self.create(width, sigma, H23)
        self.m_h4 = self.create(width, sigma, H24)

        self.m_g2a = cv2.sepFilter2D(image, cv2.CV_32F, self.m_g1, np.transpose(self.m_g2))
        self.m_g2b = cv2.sepFilter2D(image, cv2.CV_32F, self.m_g3, np.transpose(self.m_g3))
        self.m_g2c = cv2.sepFilter2D(image, cv2.CV_32F, self.m_g2, np.transpose(self.m_g1))
        self.m_h2a = cv2.sepFilter2D(image, cv2.CV_32F, self.m_h1, np.transpose(self.m_h2))
        self.m_h2b = cv2.sepFilter2D(image, cv2.CV_32F, self.m_h4, np.transpose(self.m_h3))
        self.m_h2c = cv2.sepFilter2D(image, cv2.CV_32F, self.m_h3, np.transpose(self.m_h4))
        self.m_h2d = cv2.sepFilter2D(image, cv2.CV_32F, self.m_h2, np.transpose(self.m_h1))

        #print self.m_g2a.shape, self.m_g2a.shape
        self.g2aa = np.dot(self.m_g2a,np.transpose(self.m_g2a)) # g2a*
        self.g2ab = np.dot(self.m_g2a,np.transpose(self.m_g2b))
        self.g2ac = np.dot(self.m_g2a,np.transpose(self.m_g2c))
        self.g2bb = np.dot(self.m_g2b,np.transpose(self.m_g2b)) # g2b*
        self.g2bc = np.dot(self.m_g2b,np.transpose(self.m_g2c))
        self.g2cc = np.dot(self.m_g2c,np.transpose(self.m_g2c)) # g2c*

        self.h2aa = np.dot(self.m_h2a,np.transpose(self.m_h2a)) # h2a*
        self.h2ab = np.dot(self.m_h2a,np.transpose(self.m_h2b))
        self.h2ac = np.dot(self.m_h2a,np.transpose(self.m_h2c))
        self.h2ad = np.dot(self.m_h2a,np.transpose(self.m_h2d))
        self.h2bb = np.dot(self.m_h2b,np.transpose(self.m_h2b)) # h2b*
        self.h2bc = np.dot(self.m_h2b,np.transpose(self.m_h2c))
        self.h2bd = np.dot(self.m_h2b,np.transpose(self.m_h2d))
        self.h2cc = np.dot(self.m_h2c,np.transpose(self.m_h2c)) # h2c*
        self.h2cd = np.dot(self.m_h2c,np.transpose(self.m_h2d))
        self.h2dd = np.dot(self.m_h2d,np.transpose(self.m_h2d))# h2d*

        m_c1 = 0.5*(self.g2bb) + 0.25*(self.g2ac) + 0.375*(self.g2aa + self.g2cc) + 0.3125*(self.h2aa + self.h2dd) + 0.5625*(self.h2bb + self.h2cc) + 0.375*(self.h2ac + self.h2bd)
        m_c2 = 0.5*(self.g2aa - self.g2cc) + 0.46875*(self.h2aa - self.h2dd) + 0.28125*(self.h2bb - self.h2cc) + 0.1875*(self.h2ac - self.h2bd)
        m_c3 = (-self.g2ab) - self.g2bc - (0.9375*(self.h2cd + self.h2ab)) - (1.6875*(self.h2bc)) - (0.1875*(self.h2ad))

        magnitude, angle =cv2.cartToPolar(m_c2, m_c3) # dominant orientation angle

    def Steer(self, p, theta):
        #Create the steering coefficients, then compute G2 and H2 at orientation theta:
        ct = math.cos(theta)
        ct2 = ct*ct
        ct3 = ct2*ct
        st = math.sin(theta)
        st2 = st*st
        st3 = st2*st

        ga = ct2
        gb = (-2.0 * ct * st)
        gc = st2
        ha = ct3
        hb = (-3.0 * ct2 * st)
        hc = (3.0 * ct * st2)
        hd = -st3
        g2 = ga * self.m_g2a[p[0],p[1]] + gb * self.m_g2b[p[0],p[1]] + gc * self.m_g2c[p[0],p[1]]
        h2 = ha * self.m_h2a[p[0],p[1]] + hb * self.m_h2b[p[0],p[1]] + hc * self.m_h2c[p[0],p[1]] + hd * self.m_h2d[p[0],p[1]]
        return g2*g2 + h2*h2

    def SteerImg(self, theta):
        ct = math.cos(theta)
        ct2 = ct*ct
        ct3 = ct2*ct
        st = math.sin(theta)
        st2 = st*st
        st3 = st2*st

        ga = ct2
        gb = (-2.0 * ct * st)
        gc = st2
        ha = ct3
        hb = (-3.0 * ct2 * st)
        hc = (3.0 * ct * st2)
        hd = -st3

        g2 = ga * self.m_g2a + gb * self.m_g2b + gc * self.m_g2c
        h2 = ha * self.m_h2a + hb * self.m_h2b + hc * self.m_h2c + hd * self.m_h2d
        return g2*g2 + h2*h2, g2*g2 - h2*h2

def findLineWidth(filtered_img, line_mid, theta):
    global call_counter
    filtered_img = cv2.dilate(filtered_img,dp.dilate_kernel,iterations=1) 
    img = np.zeros(np.shape(filtered_img))
    img[filtered_img > 0] = 1
    img[filtered_img < 0] = 0
    #cv2.imshow("image", img)
    #print np.sum(img)
    x, y= np.meshgrid(range(filtered_img.shape[1]), range(filtered_img.shape[0]))
    mask = np.zeros((x.shape))
    a = math.tan(theta)
    mask[abs(y - a*x + a*line_mid[1] - line_mid[0])/(math.sqrt(1+a*a)) < dp.steer_sigma] = 1
    mask_sum = np.sum(mask)
    overlap_sum = np.sum(img[mask == 1])
    
    cv2.imshow("test", mask)
    #cv2.imwrite("/home/sepehr/projects/photos/mask" + str(call_counter) + ".jpg", mask*256)

    cv2.imshow("find", img)
    return (overlap_sum/mask_sum) * dp.steer_sigma

call_counter = 0
def finderMain (data):
    global call_counter 
    call_counter = call_counter + 1
    start = time.time()
    try:
        orig = bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError, e:
        print e

    #reducing image size
    cv2.imshow("1", orig)
    #cv2.imwrite("/home/sepehr/projects/photos/orig" + str(call_counter) + ".jpg", orig)

    while orig.shape[0] > 320:
        orig = cv2.pyrDown(orig)

    img_width = orig.shape[1]
    img_heigth = orig.shape[0]

    #turn to grayscale
    orig2 = np.copy(orig)
    img = cv2.cvtColor(orig,cv2.COLOR_RGB2GRAY)

    # blur the image
    #img = cv2.GaussianBlur(img,(5,5),1)
    #cv2.imshow("smoothed", img)
    #intensify borders
    smoothed_image = cv2.GaussianBlur(img,dp.smoothing_mat_size,4)
    cv2.addWeighted(img,dp.smoothing_coeff_1,smoothed_image,dp.smoothing_coeff_2,0,dst=img)

    #setting up parameters for steer filter
    width = dp.steer_mat_width
    spacing = dp.steer_mat_spacing
    SG2 = SteerableG2(img,width,spacing)
    out = np.zeros(img.shape)

    theta = 0
    num_angles = 8
    #using only second filter-
    filtered2 = np.zeros((num_angles, img.shape[0], img.shape[1]))
    s = np.zeros(num_angles)
    for i in range(num_angles):
        theta = i * math.pi/num_angles
        _, filtered2[i] = SG2.SteerImg(theta)
        s[i] = sum(sum(abs(filtered2[i])))

    f2 = np.floor(filtered2.max(axis = 0))
    #f2 = filtered2[s == s.max(axis = 0)][0]

    t1 = np.percentile(f2,dp.f2_thresh_percent_min)
    t2 = np.percentile(f2,dp.f2_thresh_percent_max)
    f2 [f2 > t2] = 0
    f2 [f2 < t1] = 0
    f2 = 255*f2 /np.max(f2)

    #cv2.imshow('1', f2)

    dout2 = cv2.dilate(f2,dp.dilate_kernel,iterations=2)
    
    dout2 = cv2.erode(dout2,dp.dilate_kernel,iterations=2)

    for_line_width = cv2.erode(dout2,dp.dilate_kernel,iterations=1)

    i = 0
    dout2 = cv2.convertScaleAbs(dout2)
    lines = cv2.HoughLinesP(dout2,1,np.pi/180,dp.houghline_vote_thresh ,minLineLength=min(dout2.shape)*dp.houghline_min_length_coeff, maxLineGap= dp.houghline_max_gap)
    if (lines is not None):
            for x1,y1,x2,y2 in lines[0]:
                if ((x1 < img_width/7 and x2 < img_width/7) or (x1 > img_width*6/7 and x2 > img_width*6/7)):
                    continue

                cv2.line(orig2,(x1,y1),(x2,y2),(0,255,0),5)
                i += 1
                if i == 1:
                    line_loc['measure_Xm'] = (y1 + y2) / 2
                    line_loc['measure_Ym'] = (x1 + x2) / 2
                    if x1 == x2:
                        line_loc["measure_theta"] = math.pi/2
                    else:
                        line_loc['measure_theta'] = math.atan(float(y1-y2)/float(x1-x2))
                    print "measure_theta = ", line_loc['measure_theta']
                    if line_loc["measure_theta"] < 0:
                        line_loc["measure_theta"] = line_loc["measure_theta"] + math.pi
                    
                    line_width_p = findLineWidth(for_line_width, (line_loc["measure_Xm"], line_loc["measure_Ym"]), line_loc['measure_theta'])

                    #l = (img_width*dp.line_width/line_width_p)
                    #line_loc["measure_Z"] = (l/2)/math.tan(dp.camera_apparatus/2)
                    line_width_theta = dp.camera_apparatus*line_width_p/img_width
                    line_loc["measure_Z"] = (dp.line_width/2)/math.tan(line_width_theta/2)
                    break

    cv2.imshow("2", dout2)
    #cv2.imwrite("/home/sepehr/projects/photos/dout2" + str(call_counter) + ".jpg", dout2)
    cv2.imshow('3', orig2)
    #cv2.imwrite("/home/sepehr/projects/photos/orig2" + str(call_counter) + ".jpg", orig2)

    out_msg = str(line_loc)
    observer_out_pub.publish(out_msg)
    end = time.time()
    print "elapsed time =", end - start
    print "line loc, calced in linefinder", line_loc
    k = cv2.waitKey(1)


print "line_finder_2 main is running"
cv2.namedWindow("1", cv2.WINDOW_NORMAL)
cv2.namedWindow("2", cv2.WINDOW_NORMAL)
cv2.namedWindow("3", cv2.WINDOW_NORMAL)
cv2.namedWindow("find", cv2.WINDOW_NORMAL)
cv2.namedWindow("test", cv2.WINDOW_NORMAL)

line_loc = {'measure_Xm':0, 'measure_Ym':0, 'measure_theta':0, 'measure_Z': 0}
rospy.init_node('line_finder_2')
bridge = CvBridge()
#/pics
# /ardrone/bottom/image_raw
# /PowerLineVer2/quad_floor_vision
feedback_sub = rospy.Subscriber("/ardrone/bottom/image_raw", Image, finderMain, queue_size = 1, buff_size=2**24)
observer_out_pub = rospy.Publisher("ros_control/vision_out_receiver", String, queue_size = 1)
rospy.spin()